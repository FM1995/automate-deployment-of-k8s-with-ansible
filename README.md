### Automate deployment of K8s with Ansible

In this project we will see how to use terraform to deploy applications in to a Kubernetes cluster using Ansible. So in this project we will create a K8s cluster on AWS, configure Ansible to connect to EKS cluster and then deploy a Deployment and service component.

#### Lets get started

Using the project from the below

https://gitlab.com/FM1995/terraform-eks-lab/-/tree/main?ref_type=heads

Lets first initialise it

```
terraform init
```

![Image1](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image1.png)

Can then create the cluster

```
terraform apply –auto-approve
```

![Image2](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image2.png)

And can see in the console it has been created

![Image3](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image3.png)

Now using Ansible we want to connect and  deploy to our newly created cluster

Creating a new file to deploy to the cluster

![Image4](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image4.png)

Next we will configure a play to deploy to our Kubernetes cluster

And to help us do that there is a dedicated ansible module, which allows us to run kubectl commands and not even require kubectl installed on the servers

Using the below documentation, lets first create a namespace

https://docs.ansible.com/ansible/latest/collections/kubernetes/core/k8s_module.html

Can configure it like the below

![Image5](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image5.png)

Can then modify it like the below to include the hosts and then changing the names

![Image6](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image6.png)

Next thing we need to configure is which cluster and how to connect to a specific cluster and we can use the below ‘kubeconfig’ attribute which can point towards our kubeconfig path

![Image7](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image7.png)

Can then get the config using the below

```
aws eks --region eu-central-1 update-kubeconfig --name myapp-eks-cluster
```

And the config has been saved to the location /home/vboxuser/.kube/config

![Image8](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image8.png)

Implementing the config into playbook

![Image9](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image9.png)

Or to make within the same directory for simplicity

![Image10](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image10.png)

Below is the new config

![Image11](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image11.png)

Next we need to configure the pre-requisites

For executing this module, we already have a python version greater than 3.6

![Image12](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image12.png)

Can see python3 is installed

![Image13](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image13.png)

Checking if openshift is installed

```
python3 -c “import openshift”
```

![Image14](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image14.png)

Can the below to install it

```
pip3 install openshift –user
```

And can see it was successfully installed

![Image15](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image15.png)

Can see PyYAML is already installed

![Image16](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image16.png)

Can do same thing with Kubernetes and jsonpatcher

```
pip3 install “Kubernetes>=12.0.0 jsonpatch –user
```

![Image17](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image17.png)

Can now test the import

![Image18](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image18.png)

Can then configure the config file back to hosts instead of the plugin

![Image19](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image19.png)

Now lets run the playbook

```
ansible-playbook deplo-to-k8s.yaml
```

And can see a successful run

![Image20](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image20.png)

Now lets go back to the cluster and see if the namespace was created

But first we will have to export the kubeconfig

And then run the below

```
kubectl get ns
```

And can see the namespace got created

![Image21](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image21.png)

Now using this namespace lets deploy an app in to our namespace using ansible

Now lets deploy  simple nginx deployment

![Image22](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image22.png)

Can then configure a new play like the below, using another attribute called src

![Image23](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image23.png)

Can then run the playbook again

```
ansible-playbook deploy-to-k8s.yaml
```

![Image24](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image24.png)

Can then check if  the app got deployed

```
kubectl get pod -n my-app
```

And can see it is successful

![Image25](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image25.png)

Can also do the below

```
kubectl get svc -n my-app
```

![Image26](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image26.png)

Can then use the given dns to access nginx and access on the webpage

![Image27](https://gitlab.com/FM1995/automate-deployment-of-k8s-with-ansible/-/raw/main/Images/Image27.png)








